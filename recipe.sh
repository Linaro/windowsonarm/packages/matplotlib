#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    url="https://github.com/matplotlib/matplotlib/archive/refs/tags/v$version.zip"
    wget -q $url -O src.zip
    unzip -q src.zip
    cd matplotlib-$version

    cat > mplsetup.cfg << EOF
[libs]
system_freetype = True
system_qhull = True
EOF
}

dependencies()
{
    deps_root=$(cygpath -w "$(pwd)")/dependencies
    triplet=arm64-windows
    vcpkg.exe install\
        --triplet $triplet \
        --x-install-root="$deps_root" \
        qhull freetype

    deps_root="$deps_root/$triplet"
    LIB="${LIB:-}"
    export LIB="$deps_root/lib;$LIB"
    INCLUDE="${INCLUDE:-}"
    export INCLUDE="$deps_root/include;$LIB"

    # get dll needed for wheel
    deps_root="$(cygpath -u $deps_root)"
    cp -v $deps_root/bin/*.dll lib/matplotlib/
    echo "lib/matplotlib/*.dll" > MANIFEST.in
}

build()
{
    git clone -q https://gitlab.com/Linaro/windowsonarm/packagetools

    python \
        ./packagetools/scripts/cross_compile_python_wheel.py \
        --python-version 3.10.4 \
        --source-dir .
}

package()
{
    mv dist/*.whl $out_dir
}

checkout
dependencies
build
package
